const http = require('http');

const config = require('../config/system.config.json');
const logger = require('../src/utils/logger');

if (!config) {
    logger.fatal('error occurred while reading application configuration. quitting...');
    process.exit(1);
}

const app = require('../src/app');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const HOST = config.host;
const HTTP_PORT = config.port;

const httpServer = http.createServer(app);

httpServer.listen(HTTP_PORT, HOST, () => {
    logger.info(`http server is listening on port  ${HOST}:${HTTP_PORT}`);
});

process.on('uncaughtException', (err) => {
    logger.fatal('uncaught exception occurred and application has to be closed');
    logger.fatal(err);
})
    .on('unhandledRejection', (reason, p) => {
        logger.error(`${JSON.stringify(reason)} Unhandled Rejection at Promise ${JSON.stringify(p)}`);
    });
