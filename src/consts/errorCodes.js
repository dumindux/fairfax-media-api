module.exports = {
    GENERIC_ERROR: {
        code: 0,
        description: 'Internal server error occured'
    },
    URL_NOT_FOUND: {
        code: 1,
        description: 'Url not found'
    },
    OBJECT_NOT_FOUND: {
        code: 2,
        description: 'Requested object not found'
    },
    INPUT_VALIDATION_ERROR: {
        code: 3,
        description: 'Input validation error'
    },
    ALREADY_EXISTS: {
        code: 4,
        description: 'Object already exists'
    }
};
