const express = require('express');
const { check } = require('express-validator/check');

const { postArticle, getArticle } = require('../routeHandlers/articlesRouteHandlers');

const router = express.Router();

router.post('/', [
    check('id').exists().isString(), // validate id
    check('title').exists().isString(), // validate title
    check('body').exists().isString(), // validate body
    check('tags').exists().isArray(), // validate tags for array
    check('tags.*').isString(), // validate tag for string
    check('date').exists().isString().matches(/^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$/) // validate date
], postArticle);

router.get('/:id', [
    check('id').exists().isString() // validate id
], getArticle);

module.exports = router;
