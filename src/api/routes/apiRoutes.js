const express = require('express');
const articlesRoute = require('./articlesRoutes');
const tagsRoute = require('./tagRoutes');

const router = express.Router();

router.use('/articles', articlesRoute);
router.use('/tags', tagsRoute);

module.exports = router;
