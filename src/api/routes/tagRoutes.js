const express = require('express');
const { check } = require('express-validator/check');
const { getArticlesByDateAndTag } = require('../routeHandlers/tagsRouteHandlers');

const router = express.Router();

router.get('/:tagName/:date', [
    check('tagName').exists().isString(), // validate tagname
    check('date').exists().isString().matches(/^[0-9]{4}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$/) // validate date
], getArticlesByDateAndTag);

module.exports = router;
