const { validationResult } = require('express-validator/check');

const ArticlesController = require('../controllers/ArticlesController');
const TestApiError = require('../../utils/error/TestApiError');
const ERROR_CODES = require('../../consts/errorCodes');
const SuccessMessage = require('../../utils/messages/SuccessMessage');
const HTTP_STATUS_CODES = require('../../consts/httpStatusCodes');
const logger = require('../../utils/logger');

/**
 * articlesRouteHandlers module
 * @module api/routeHandlers/articlesRouteHanlders
 */

/**
 * Router handlers defined separately to improve modularity and testability of code
 */

/**
 * Handle POST articles endpoint
 * @param {object} req - HTTP request
 * @param {object} res - HTTP response
 * @param {function} next - call to next middleware
 */
async function postArticle(req, res, next) {
    try {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            const result = await (new ArticlesController(req.app.get('articles'))).insertArticle(req.body);
            logger.debug(`articlesRouteHanlders result from controller ${JSON.stringify(result)}`);
            if (!result) {
                throw new TestApiError(ERROR_CODES.GENERIC_ERROR, null);
            }
            res.status(HTTP_STATUS_CODES.SUCCESS).send(new SuccessMessage('Successfully inserted the article'));
        } else {
            throw new TestApiError(ERROR_CODES.INPUT_VALIDATION_ERROR, errors.array());
        }
    } catch (error) {
        next(error); // forward error to error handling middleware
    }
}

/**
 * Handle GET articles/{id} endpoint
 * @param {object} req - HTTP request
 * @param {object} res - HTTP response
 * @param {function} next - call to next middleware
 */
async function getArticle(req, res, next) {
    try {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            const result = await (new ArticlesController(req.app.get('articles'))).getArticleByID(req.params.id);
            logger.debug(`articlesRouteHanlders result from controller ${JSON.stringify(result)}`);
            res.status(HTTP_STATUS_CODES.SUCCESS).send(result);
        } else {
            throw new TestApiError(ERROR_CODES.INPUT_VALIDATION_ERROR, errors.array());
        }
    } catch (error) {
        next(error);
    }
}

module.exports = {
    postArticle,
    getArticle
};
