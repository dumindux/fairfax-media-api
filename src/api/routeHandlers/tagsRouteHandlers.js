const { validationResult } = require('express-validator/check');

const ArticlesController = require('../controllers/ArticlesController');
const TestApiError = require('../../utils/error/TestApiError');
const ERROR_CODES = require('../../consts/errorCodes');
const HTTP_STATUS_CODES = require('../../consts/httpStatusCodes');
const logger = require('../../utils/logger');

/**
 * tagsRouteHandlers modulle
 * @module api/routeHandlers/tagsRouteHanlders
 */

/**
 * Router handlers defined separately to improve modularity and testability of code
 */

/**
 * Handle GET articles/{tagName}/{date} endpoint
 * @param {object} req - HTTP request
 * @param {object} res - HTTP response
 * @param {function} next - call to next middleware
 */
async function getArticlesByDateAndTag(req, res, next) {
    try {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            const result = await (new ArticlesController(req.app.get('articles'))).getArticlesByDateAndTag(req.params.date, req.params.tagName);
            logger.debug(`tagsRouteHanlders result from controller ${JSON.stringify(result)}`);
            res.status(HTTP_STATUS_CODES.SUCCESS).send(result);
        } else {
            throw new TestApiError(ERROR_CODES.INPUT_VALIDATION_ERROR, errors.array());
        }
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getArticlesByDateAndTag
};
