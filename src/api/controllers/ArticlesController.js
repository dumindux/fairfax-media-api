
const ERROR_CODES = require('../../consts/errorCodes');
const TestApiError = require('../../utils/error/TestApiError');

/**
 * Controller for all article related routes
 * This module sanitizes inputs and calls necessary methods in articles object to get desired results
 * This is used from the routing layer
 */
class ArticlesController {
    /**
     * Articles controller
     * @constructor
     * @param {object} articles - Articles store to be used
     */
    constructor(articles) {
        this.articles = articles;
    }

    /**
     * Inserts an article to the service
     * @param {object} article - Article to be inserted
     * @returns {Promise<boolean>} - Success status of the request
     */
    async insertArticle(article) {
        const { id, tags } = article;
        const date = article.date.split('-').join('');
        return this.articles.insertArticle(article, id, date, tags);
    }

    /**
     * Retrive and article for a given id
     * @param {string} id - Id of the article to be recieved
     * @returns {Promise<object>} - Article object
     */
    async getArticleByID(id) {
        const article = await this.articles.getArticleById(id);
        if (!article) {
            throw new TestApiError(ERROR_CODES.OBJECT_NOT_FOUND, null);
        }
        return article;
    }

    /**
     * Returns article information that contains the given tag within the fiven date
     * @param {string} date - Date that nees to be searched in the format YYYY-MM-DD or YYYYMMDD
     * @param {string} tag - Tag that needs to be searched
     * @returns {Promise<object>} - Article information for the search
     */
    async getArticlesByDateAndTag(date, tag) {
        const dateSanitized = date.split('-').join('');
        const filterdArticles = await this.articles.getArticlesByDateAndTag(dateSanitized, tag);
        const count = filterdArticles.length;
        const lastTestIds = filterdArticles.slice(-10).map(article => article.id);
        const relatedTags = await this.articles.getRelatedTagsForDateAndTag(dateSanitized, tag);

        return {
            tag,
            count,
            articles: lastTestIds,
            related_tags: relatedTags
        };
    }
}

module.exports = ArticlesController;
