class ErrorMessage {
    constructor(message, data, code) {
        this.success = false;
        this.code = (code !== null && code !== undefined ? code : -1);
        this.message = message || '';
        this.data = data || {};
    }
}

module.exports = ErrorMessage;
