const BunyanLogger = require('bunyan');

class Logger extends BunyanLogger {
    trace(str, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.trace(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }

            super.trace(logString);
        } catch (err) {
            // do nothing
        }
    }

    debug(str, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.debug(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }
            super.debug(logString);
        } catch (err) {
            // do nothing
        }
    }

    info(str, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.info(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }
            super.info(logString);
        } catch (err) {
            // do nothing
        }
    }

    warn(str, errorObj, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.warn(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }
            super.warn(logString);

            if (errorObj) {
                super.warn(errorObj);
            }
        } catch (err) {
            // do nothing
        }
    }

    error(str, errorObj, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.error(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }
            super.error(logString);

            if (errorObj) {
                super.error(errorObj);
            }
        } catch (err) {
            // do nothing
        }
    }

    fatal(str, errorObj, messageHeader) {
        try {
            let logString = str;

            if (str.constructor.name === 'Object') {
                super.fatal(JSON.stringify(str));
                return;
            }

            if (messageHeader) {
                logString = `[${messageHeader}] ${logString}`;
            }
            super.fatal(logString);

            if (errorObj) {
                super.fatal(errorObj);
            }
        } catch (err) {
            // do nothing
        }
    }
}

module.exports = Logger;
