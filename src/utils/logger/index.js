const Logger = require('./Logger');
const loggerConfig = require('./../../../config/logger.config.json');

module.exports = new Logger({
    name: loggerConfig.logHeader,
    streams: [
        {
            level: loggerConfig.level,
            stream: process.stdout
        }
    ]
});
