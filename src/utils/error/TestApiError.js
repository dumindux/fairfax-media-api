const ERROR_CODES = require('../../consts/errorCodes');

class TestApiError extends Error {
    constructor(error, innerErrorObj) {
        const _defaultOptions = {
            error: ERROR_CODES.GENERIC_ERROR,
            innerErrorObj: null
        };

        const options = Object.assign({}, _defaultOptions, {
            error,
            innerErrorObj
        });

        super(options.error.description);
        this.error = options.error;
        this.innerErrorObj = options.innerErrorObj;
    }
}

module.exports = TestApiError;
