const express = require('express');
const bodyParser = require('body-parser');
const swagger = require('swagger-ui-express');

const apiRoutes = require('./api/routes/apiRoutes');
const Articles = require('./core/data/Articles');
const ArticlesDao = require('./database/ArticlesDao');
const logger = require('./utils/logger');
const swaggerDoc = require('./swagger');
const notFoundHandler = require('./middleware/404handler');
const errorHandler = require('./middleware/errorHandler');

const app = express();
const databaseConnection = new ArticlesDao();

app.set('articles', new Articles(databaseConnection)); // attach Articles dependency to the app

const loggerMiddleware = (req, res, next) => {
    logger.debug(`Incoming HTTP Request ${req.method} ${req.url}`); // log each incoming request in debug level
    next();
};

app.use(loggerMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.disable('x-powered-by');

app.use('/api-docs', swagger.serve, swagger.setup(swaggerDoc)); // mount swagger endpoint

app.use('/', apiRoutes); // base router for the api
app.use(notFoundHandler); // middleware for handling url not found error
app.use(errorHandler); // middleware for handling errors

module.exports = app;
