/**
 * Handles operations related to articles
 */
class Articles {
    /**
     * Articles.
     * @constructor
     * @param {Object} articlesDao - Database access object for articles
     */
    constructor(articlesDao) {
        this.articlesDao = articlesDao;
    }

    /**
     * Inserts an article to the service
     * @param {Object} article - Article to be inserted
     * @param {string} id - Id of the article
     * @param {string} date - Date for the article
     * @param {Array} tags - tags for the article
     * @returns {Promise<boolean>} - Success status of the request
     */
    async insertArticle(article, id, date, tags) {
        if (!article || !id || !date || !tags) {
            return false;
        }
        let status = await this.articlesDao.insertToArticlesIdMapped(article, id);
        status = status && (await this.articlesDao.insertToArticlesDateTagMapped(article, date, tags));
        status = status && (await this.articlesDao.insertToRelatedTagsDateTagMapped(article, date, tags));
        return status;
    }

    /**
     * Get an article given the Id
     * @param {string} id - Id of the article
     * @returns {Promise<object>} - Article with the given id
     */
    async getArticleById(id) {
        const article = await this.articlesDao.getFromArticlesIdMapped(id);
        if (!article) {
            return null;
        }
        return article;
    }

    /**
     * Get an article list given a date and a tag
     * @param {string} date - Date for the search
     * @param {string} tag - Tag for the search
     * @returns {Promise<Array>} - Article array for the search
     */
    async getArticlesByDateAndTag(date, tag) {
        const articles = await this.articlesDao.getFromArticlesDateTagMapped(date, tag);
        if (!articles) {
            return [];
        }
        return articles;
    }

    /**
     * Get an related tag set given a date and a tag
     * @param {string} date - Date for the search
     * @param {string} tag - Tag for the search
     * @returns {Promise.<Array>} - Related tag array for the search
     */
    async getRelatedTagsForDateAndTag(date, tag) {
        const relatedTags = await this.articlesDao.getFromRelatedTagsDateTagMapped(date, tag);
        if (!relatedTags) {
            return [];
        }
        return [...relatedTags];
    }
}

module.exports = Articles;
