const ERROR_CODES = require('../consts/errorCodes');
const HTTP_STATUS_CODES = require('../consts/httpStatusCodes');
const ErrorMessage = require('../utils/messages/ErrorMessage');
const TestApiError = require('../utils/error/TestApiError');
const logger = require('../utils/logger');

/**
 * errorHandler module
 * @module api/middleware/errorHandler
 */

/**
 * Generic error handler
 * @function errorHandler
 * @param {object} err - Error object
 * @param {object} req - HTTP request
 * @param {object} res - HTTP response
 * @param {function} next - call to next middleware
 */
module.exports = (err, req, res, next) => {
    logger.error(`errorHandler an error occurred ${JSON.stringify(err)}`);

    if (err instanceof TestApiError) {
        let errorObject = null;
        switch (err.error.code) {
            case ERROR_CODES.URL_NOT_FOUND.code:
                res.status(HTTP_STATUS_CODES.NOT_FOUND).send(new ErrorMessage(ERROR_CODES.URL_NOT_FOUND.description, errorObject, ERROR_CODES.URL_NOT_FOUND.code));
                break;
            case ERROR_CODES.OBJECT_NOT_FOUND.code:
                res.status(HTTP_STATUS_CODES.NOT_FOUND).send(new ErrorMessage(ERROR_CODES.OBJECT_NOT_FOUND.description, errorObject, ERROR_CODES.OBJECT_NOT_FOUND.code));
                break;
            case ERROR_CODES.INPUT_VALIDATION_ERROR.code:
                errorObject = [];
                err.innerErrorObj.forEach((error) => {
                    const paramSanitized = error.param.split(/[[\]]/)[0];
                    switch (paramSanitized) {
                        case 'id':
                            errorObject.push({ field: 'id', description: 'id should be a string' });
                            break;
                        case 'date':
                            if (error.location === 'body') {
                                errorObject.push({ field: 'date', description: 'date should be of format YYYY-MM-DD' });
                            } else {
                                errorObject.push({ field: 'date', description: 'date should be of format YYYYMMDD' });
                            }
                            break;
                        case 'title':
                            errorObject.push({ field: 'title', description: 'title should be a string' });
                            break;
                        case 'body':
                            errorObject.push({ field: 'body', description: 'body should be a string' });
                            break;
                        case 'tags':
                            errorObject.push({ field: 'tags', description: 'tags should be an array of strings' });
                            break;
                        default:
                    }
                });
                res.status(HTTP_STATUS_CODES.BAD_REQUEST).send(new ErrorMessage(ERROR_CODES.INPUT_VALIDATION_ERROR.description, { errors: errorObject },
                    ERROR_CODES.INPUT_VALIDATION_ERROR.code));
                break;
            default:
                res.status(HTTP_STATUS_CODES.INTERNAL_ERROR).send(new ErrorMessage(ERROR_CODES.GENERIC_ERROR.description, errorObject, ERROR_CODES.GENERIC_ERROR.code));
        }
    } else {
        res.status(HTTP_STATUS_CODES.INTERNAL_ERROR).send(new ErrorMessage(ERROR_CODES.GENERIC_ERROR.description, null, ERROR_CODES.GENERIC_ERROR.code));
    }
};
