const ERROR_CODES = require('../consts/errorCodes');
const TestApiError = require('../utils/error/TestApiError');
const logger = require('../utils/logger');

/**
 * 404handler module
 * @module api/middleware/404handler
 */

/**
 * Throw url not found error to be handled by the error handling middleware
 * @function 404handler
 * @param {object} req - HTTP request
 * @param {object} res - HTTP response
 * @param {function} next - call to next middleware
 */
module.exports = (req, res, next) => {
    logger.error('404handler url not found');
    next(new TestApiError(ERROR_CODES.URL_NOT_FOUND, 'Url not found', null));
};
