module.exports = {
    swagger: '2.0',
    info: {
        description: 'Article management service',
        version: '1.0.0',
        title: 'Fairfax media test exercise'
    },
    host: 'localhost:4444',
    basePath: '/',
    tags: [
        {
            name: 'Articles',
            description: 'Creating and searching articles'
        }
    ],
    schemes: [
        'http'
    ],
    paths: {
        '/articles': {
            post: {
                summary: 'Save a new article',
                tags: [
                    'Articles'
                ],
                description: 'Save a new article given in the body to the service. If an article exists for the id an error will be returned',
                produces: [
                    'application/json'
                ],
                consumes: [
                    'application/json'
                ],
                parameters: [
                    {
                        in: 'body',
                        name: 'body',
                        required: true,
                        schema: {
                            $ref: '#/definitions/article'
                        }
                    }
                ],
                responses: {
                    200: {
                        description: 'Success',
                        schema: {
                            $ref: '#/definitions/successMessage'
                        }
                    },
                    500: {
                        description: 'Operation failed',
                        schema: {
                            $ref: '#/definitions/internalServerError'
                        }
                    },
                    400: {
                        description: 'Bad request',
                        schema: {
                            $ref: '#/definitions/badRequest'
                        }
                    }
                }
            }
        },
        '/articles/{id}': {
            get: {
                tags: [
                    'Articles'
                ],
                summary: 'Get the article with the given id',
                description: 'Get an article with the given id. An error will be returned in the id is not found',
                produces: [
                    'application/json'
                ],
                parameters: [
                    {
                        name: 'id',
                        description: 'Id for the article',
                        in: 'path',
                        required: true,
                        type: 'string',
                        example: '2'
                    }
                ],
                responses: {
                    200: {
                        description: 'Successful operation',
                        schema: {
                            $ref: '#/definitions/article'
                        }
                    },
                    500: {
                        description: 'Operation failed',
                        schema: {
                            $ref: '#/definitions/internalServerError'
                        }
                    },
                    404: {
                        description: 'Not found',
                        schema: {
                            $ref: '#/definitions/objectNotFound'
                        }
                    },
                    400: {
                        description: 'Bad request',
                        schema: {
                            $ref: '#/definitions/badRequest'
                        }
                    }
                }
            }
        },
        '/tags/{tagName}/{date}': {
            get: {
                summary: 'Get article information by tag and date',
                tags: [
                    'Articles'
                ],
                description: 'Get article information provided a specific date and a tag name',
                produces: [
                    'application/json'
                ],
                parameters: [
                    {
                        name: 'tagName',
                        description: 'Arbitrary tag name',
                        in: 'path',
                        required: true,
                        type: 'string',
                        example: 'health'
                    },
                    {
                        name: 'date',
                        description: 'Date in the format YYYYMMDD',
                        in: 'path',
                        required: true,
                        type: 'string',
                        example: '20160922',
                        pattern: '^[0-9]{4}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$'
                    }
                ],
                responses: {
                    200: {
                        description: 'success',
                        schema: {
                            $ref: '#/definitions/dateTagInformation'
                        }
                    },
                    500: {
                        description: 'Operation failed',
                        schema: {
                            $ref: '#/definitions/internalServerError'
                        }
                    },
                    400: {
                        description: 'Bad request',
                        schema: {
                            $ref: '#/definitions/badRequest'
                        }
                    }
                }
            }
        }
    },
    definitions: {
        article: {
            type: 'object',
            properties: {
                id: {
                    type: 'string',
                    example: '2',
                    required: true
                },
                title: {
                    type: 'string',
                    example: 'latest science shows that potato chips are better for you than sugar',
                    required: true
                },
                date: {
                    type: 'string',
                    pattern: '^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$',
                    example: '2016-09-22',
                    required: true
                },
                body: {
                    type: 'string',
                    example: 'some text, potentially containing simple markup about how potato chips are great',
                    required: true
                },
                tags: {
                    type: 'array',
                    items: {
                        type: 'string'
                    },
                    example: ['health', 'fitness', 'science'],
                    required: true
                }
            }
        },
        dateTagInformation: {
            type: 'object',
            properties: {
                tag: {
                    type: 'string',
                    example: 'health'
                },
                count: {
                    type: 'integer',
                    example: 17
                },
                articles: {
                    type: 'array',
                    items: {
                        type: 'string'
                    },
                    example: ['1', '7']
                },
                related_tags: {
                    type: 'array',
                    items: {
                        type: 'string'
                    },
                    example: ['fitness', 'science']
                }
            }
        },
        objectNotFound: {
            type: 'object',
            properties: {
                success: {
                    type: 'boolean',
                    example: false
                },
                code: {
                    type: 'integer',
                    example: 2
                },
                message: {
                    type: 'string',
                    example: 'Requested object not found'
                },
                data: {
                    type: 'object',
                    example: {}
                }
            }
        },
        internalServerError: {
            type: 'object',
            properties: {
                success: {
                    type: 'boolean',
                    example: false
                },
                code: {
                    type: 'integer',
                    example: 0
                },
                message: {
                    type: 'string',
                    example: 'Internal server error'
                },
                data: {
                    type: 'object',
                    example: {}
                }
            }
        },
        successMessage: {
            type: 'object',
            properties: {
                success: {
                    type: 'boolean',
                    example: true
                },
                message: {
                    type: 'string',
                    example: 'Successfully inserted the article'
                },
                data: {
                    type: 'object',
                    example: {}
                }
            }
        },
        badRequest: {
            type: 'object',
            properties: {
                success: {
                    type: 'boolean',
                    example: false
                },
                code: {
                    type: 'integer',
                    example: 3
                },
                message: {
                    type: 'string',
                    example: 'Input validation error'
                },
                data: {
                    type: 'object',
                    example: {
                        errors: [
                            {
                                field: 'date',
                                description: 'date should be of format YYYYMMDD'
                            }
                        ]
                    }
                }
            }
        }
    }
};
