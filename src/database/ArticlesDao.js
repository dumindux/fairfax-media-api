const TestApiError = require('../utils/error/TestApiError');
const ERROR_CODES = require('../consts/errorCodes');

/**
 * A simulation of a database access layer. The database could be an in memory
 * database like Redis for faster reads, backed by a persistent database.
 *
 * data structures used are below,
 * articlesIdMapped - Map{id -> article}
 * articlesDateTagMapped - Map{date -> Map{tag -> [article1, article2]}}
 * relatedTagsDateTagMapped - Map{date -> Map{tag -> Set(tags)}}
 */
class ArticlesDao {
    /**
     * ArticlesDao.
     * @constructor
     * @param {Object} articles - Articles store to be used
     */
    constructor() {
        this.articlesIdMapped = new Map(); // maintain a id to article mapping
        this.articlesDateTagMapped = new Map(); // maintain a date -> tag -> article mapping for faster reads for articles with certain date and tags
        this.relatedTagsDateTagMapped = new Map(); // maintain a date -> tag -> related-tag-for-the-day mapping for faster relatedtag reads
    }

    /**
     * Inserts an article to the service
     * @param {Object} article - Article to be inserted
     * @param {string} id - Id of the article
     * @returns {Promise<boolean>} - Success status of the request
     */
    async insertToArticlesIdMapped(article, id) {
        if (!id || !article) {
            return false;
        }
        if (this.articlesIdMapped.has(id)) {
            throw new TestApiError(ERROR_CODES.ALREADY_EXISTS, null);
        }
        this.articlesIdMapped.set(id, article);
        return true;
    }

    /**
     * Get an article given the Id
     * @param {string} id - Id of the article
     * @returns {Promise<object>} - Article with the given id
     */
    async getFromArticlesIdMapped(id) {
        return this.articlesIdMapped.get(id);
    }

    /**
     * Inserts an article to date -> tag -> article mapping
     * @param {Object} article - Article to be inserted
     * @param {string} date - Date of the article
     * @param {Array} tags - Tags for the article
     * @returns {Promise<boolean>} - Success status of the request
     */
    async insertToArticlesDateTagMapped(article, date, tags) {
        if (!article || !date || !tags) {
            return false;
        }
        let articlesForTheDate = this.articlesDateTagMapped.get(date);
        if (!articlesForTheDate) {
            articlesForTheDate = new Map();
            tags.forEach((tag) => {
                articlesForTheDate.set(tag, [article]);
            });
            this.articlesDateTagMapped.set(date, articlesForTheDate);
        } else {
            tags.forEach((tag) => {
                const articlesArray = articlesForTheDate.get(tag);
                if (!articlesArray) {
                    articlesForTheDate.set(tag, [article]);
                } else {
                    articlesArray.push(article);
                }
            });
        }
        return true;
    }

    /**
     * Get an article list given a date and a tag
     * @param {string} date - Date for the search
     * @param {string} tag - Tag for the search
     * @returns {Promise<Array>} - Article array for the search
     */
    async getFromArticlesDateTagMapped(date, tag) {
        const articlesForTheDate = this.articlesDateTagMapped.get(date);
        if (!articlesForTheDate) {
            return articlesForTheDate;
        }
        return articlesForTheDate.get(tag);
    }

    /**
     * Inserts an article to date -> tag -> relatedTags mapping
     * @param {Object} article - Article to be inserted
     * @param {string} date - Date of the article
     * @param {Array} tags - Tags for the article
     * @returns {Promise<boolean>} - Success status of the request
     */
    async insertToRelatedTagsDateTagMapped(article, date, tags) {
        if (!article || !date || !tags) {
            return false;
        }
        let relatedTagMapForTheDate = this.relatedTagsDateTagMapped.get(date);
        if (!relatedTagMapForTheDate) {
            relatedTagMapForTheDate = new Map();
            tags.forEach((tag) => {
                const relatedTags = new Set(tags);
                relatedTags.delete(tag);
                relatedTagMapForTheDate.set(tag, relatedTags);
            });
            this.relatedTagsDateTagMapped.set(date, relatedTagMapForTheDate);
        } else {
            tags.forEach((tag) => {
                let relatedTags = relatedTagMapForTheDate.get(tag);
                if (!relatedTags) {
                    relatedTags = new Set(tags);
                    relatedTags.delete(tag);
                    relatedTagMapForTheDate.set(tag, relatedTags);
                } else {
                    tags.map(relatedTags.add.bind(relatedTags));
                    relatedTags.delete(tag);
                }
            });
        }
        return true;
    }

    /**
     * Get an related tag set given a date and a tag
     * @param {string} date - Date for the search
     * @param {string} tag - Tag for the search
     * @returns {Promise.<Set>} - Related tag set for the search
     */
    async getFromRelatedTagsDateTagMapped(date, tag) {
        const relatedTagMapForTheDate = this.relatedTagsDateTagMapped.get(date);
        if (!relatedTagMapForTheDate) {
            return relatedTagMapForTheDate;
        }
        return relatedTagMapForTheDate.get(tag);
    }
}

module.exports = ArticlesDao;
