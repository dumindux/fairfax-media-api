const request = require('supertest');
const { expect } = require('chai');
const app = require('../../src/app');
const HTTP_STATUS_CODES = require('../../src/consts/httpStatusCodes');

describe('Articles router test', () => {
    it('insert an article', (done) => {
        request(app)
            .post('/articles')
            .send({
                id: '5',
                title: 'latest science shows that potato chips are better for you than sugar',
                date: '2016-09-22',
                body: 'some text, potentially containing simple markup about how potato chips are great',
                tags: ['health', 'fitneses', 'sciencer']
            })
            .expect('Content-Type', /json/)
            .expect(HTTP_STATUS_CODES.SUCCESS)
            .then((res) => {
                expect(res.body).to.deep.equal({
                    success: true,
                    message: 'Successfully inserted the article',
                    data: {}
                });
                done();
            })
            .catch(done);
    });

    it('get an article with invalid id', (done) => {
        request(app)
            .get('/articles/1')
            .expect('Content-Type', /json/)
            .expect(HTTP_STATUS_CODES.NOT_FOUND)
            .then((res) => {
                expect(res.body).to.deep.equal({
                    success: false,
                    code: 2,
                    message: 'Requested object not found',
                    data: {}
                });
                done();
            })
            .catch(done);
    });
});
