/* eslint-disable no-unused-expressions */

require('chai').should();
const { expect } = require('chai');
const Articles = require('../../../../src/core/data/Articles');
const ArticlesDao = require('../../../../src/database/ArticlesDao');

describe('Articles tests', () => {
    it('should successfully insert an article to an articles object', async () => {
        const articles = new Articles(new ArticlesDao());

        const article = {
            id: '5',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'fitneses', 'sciencer']
        };
        const status = await articles.insertArticle(article, article.id, '20160922', article.tags);
        status.should.be.true;
    });

    it('should successfully retrieve an article given an id to articles object', async () => {
        const articles = new Articles(new ArticlesDao());

        const article = {
            id: '5',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'fitneses', 'sciencer']
        };
        const status = await articles.insertArticle(article, article.id, '20160922', article.tags);
        const returnedArticle = await articles.getArticleById(article.id);

        status.should.be.true;
        expect(returnedArticle).to.deep.equal(article);
    });

    it('should successfully retrieve articles for a given tag/date', async () => {
        const articles = new Articles(new ArticlesDao());

        const article1 = {
            id: '1',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'fitness', 'science']
        };

        const article2 = {
            id: '2',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-23',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'potato']
        };

        const article3 = {
            id: '3',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'chips', 'sugar']
        };

        const article4 = {
            id: '4',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-23',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['modern', 'latest']
        };
        const status1 = await articles.insertArticle(article1, article1.id, '20160922', article1.tags);
        const status2 = await articles.insertArticle(article2, article2.id, '20160923', article2.tags);
        const status3 = await articles.insertArticle(article3, article3.id, '20160922', article3.tags);
        const status4 = await articles.insertArticle(article4, article4.id, '20160923', article4.tags);

        const returnedArticles1 = await articles.getArticlesByDateAndTag('20160922', 'health');
        const returnedArticles2 = await articles.getArticlesByDateAndTag('20160923', 'health');

        status1.should.be.true;
        status2.should.be.true;
        status3.should.be.true;
        status4.should.be.true;

        expect(returnedArticles1[0]).to.deep.equal(article1);
        expect(returnedArticles1[1]).to.deep.equal(article3);
        expect(returnedArticles2[0]).to.deep.equal(article2);
    });

    it('should successfully retrieve related tags for a given tag/date', async () => {
        const articles = new Articles(new ArticlesDao());

        const article1 = {
            id: '1',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'fitness', 'science']
        };

        const article2 = {
            id: '2',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-23',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'potato']
        };

        const article3 = {
            id: '3',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'chips', 'sugar']
        };

        const article4 = {
            id: '4',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-23',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['modern', 'latest']
        };
        const status1 = await articles.insertArticle(article1, article1.id, '20160922', article1.tags);
        const status2 = await articles.insertArticle(article2, article2.id, '20160923', article2.tags);
        const status3 = await articles.insertArticle(article3, article3.id, '20160922', article3.tags);
        const status4 = await articles.insertArticle(article4, article4.id, '20160923', article4.tags);

        const relatedTags1 = await articles.getRelatedTagsForDateAndTag('20160922', 'health');
        const relatedTags2 = await articles.getRelatedTagsForDateAndTag('20160923', 'health');

        status1.should.be.true;
        status2.should.be.true;
        status3.should.be.true;
        status4.should.be.true;

        expect(relatedTags1).to.deep.equal(['fitness', 'science', 'chips', 'sugar']);
        expect(relatedTags2).to.deep.equal(['potato']);
    });

    it('should return false if the id is invalid', async () => {
        const articles = new Articles(new ArticlesDao());

        const article = {
            id: '5',
            title: 'latest science shows that potato chips are better for you than sugar',
            date: '2016-09-22',
            body: 'some text, potentially containing simple markup about how potato chips are great',
            tags: ['health', 'fitneses', 'sciencer']
        };
        const status = await articles.insertArticle(article, null, '20160922', article.tags);

        status.should.be.false;
    });

    it('should return null if the article is not found', async () => {
        const articles = new Articles(new ArticlesDao());

        const returnedArticle = await articles.getArticleById('5');

        (returnedArticle === null).should.be.true;
    });

    it('should return empty aray if the article information is not found', async () => {
        const articles = new Articles(new ArticlesDao());

        const returnedArticle = await articles.getArticlesByDateAndTag('19920922', 'ert');

        returnedArticle.length.should.be.equal(0);
    });

    it('should return empty aray if related tag information is not found', async () => {
        const articles = new Articles(new ArticlesDao());

        const returnedArticle = await articles.getRelatedTagsForDateAndTag('19920922', 'ert');

        returnedArticle.length.should.be.equal(0);
    });
});
